<?php

if (!defined('\Project\Tools\Modules\IS_START')) {
    include_once(dirname(__DIR__) . '/project.tools/include.php');
}

include_once(dirname(__DIR__) . '/lib/config.php');

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Project\Tools\Modules;

Loc::loadMessages(__FILE__);

class jerff_ajax extends CModule
{

    public $MODULE_ID = 'jerff.ajax';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $PARTNER_NAME;
    public $PARTNER_URI;
    const MODULE_CODE = 'jerff';
    const PARTNER_CODE = '';
    const MIN_MAIN_VERSION = '16.0.0';
    const REQUIRE_MODULES = ['main', 'iblock'];

    use Modules\Install;

    const FILES = [
        '/components/' => '/bitrix/components/',
        '/site/public/project.ajax/' => '/project.ajax/',
        '/site/templates/' => '/bitrix/templates/',
    ];

    function __construct()
    {
        $this->setParam(__DIR__, 'JERFF_AJAX');
        $this->MODULE_NAME = GetMessage('JERFF_AJAX_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('JERFF_AJAX_DESCRIPTION');
        $this->PARTNER_NAME = GetMessage('JERFF_AJAX_PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('JERFF_AJAX_PARTNER_URI');
    }

    public function DoInstall()
    {
        $this->Install();
    }

    public function DoUninstall()
    {
        $this->Uninstall();
    }

    /*
    * InstallDB
    */

    public function InstallDB()
    {
        Loader::includeModule('iblock');

        $res = CSite::GetList();
        $arSite = array();
        while ($arItem = $res->Fetch()) {
            $arSite[] = $arItem['LID'];
        }
        \Project\Tools\Iblock\Update\Add::iblock(
            $arSite,
            include __DIR__ . '/iblock/iblocktype.php',
            include __DIR__ . '/iblock/form/iblock.php',
            include __DIR__ . '/iblock/form/iblockproperty.php'
        );
        \Project\Tools\Iblock\Update\Add::generateElement(
            $arSite,
            include __DIR__ . '/iblock/iblocktype.php',
            include __DIR__ . '/iblock/ajax/iblock.php'
        );
    }

    public function UnInstallDB()
    {

    }

    /*
     * InstallFiles
     */

    public function InstallFiles($arParams = array())
    {
        foreach (self::FILES as $key => $value) {
            CopyDirFiles(__DIR__ . $key, Application::getDocumentRoot() . $value, true, true);
        }
    }

    public function UnInstallFiles()
    {
        foreach (self::FILES as $key => $value) {
            DeleteDirFiles(__DIR__ . $key, Application::getDocumentRoot() . $value);
        }
    }

}
