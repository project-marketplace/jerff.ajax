<?php

return array(
    'IBLOCK_TYPE_ID' => \Jerff\Ajax\Config::IBLOCK,
    'NAME' => \Jerff\Ajax\Config::IBLOCK_AJAX,
    'CODE' => \Jerff\Ajax\Config::IBLOCK_AJAX,
    'LIST_PAGE_URL' => '',
    'DETAIL_PAGE_URL' => '',
    'CANONICAL_PAGE_URL' => '',
    'RIGHTS_MODE' => 'S',
    'ACTIVE' => 'Y',
    'REST_ON' => 'N',
    'WF_TYPE' => 'N',
    'INDEX_ELEMENT' => 'N',
    'INDEX_SECTION' => 'N',
    'LID' => $arSite,
    'FIELDS' => array(
        'ACTIVE_FROM' =>
            array(
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => '=now',
            ),
    ),
    'SORT' => 500,
    'GROUP_ID' => array(
        2 => 'R',
        1 => 'X',
    ),
    'VERSION' => 2,
);