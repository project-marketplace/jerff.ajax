<?php

return array(
    'ID' => \Jerff\Ajax\Config::IBLOCK,
    'EDIT_FILE_BEFORE' => '',
    'EDIT_FILE_AFTER' => '',
    'IN_RSS' => 'N',
    'SECTIONS' => 'Y',
    'SORT' => 500,
    'LANG' => array(
        'ru' => array(
            'NAME' => \Jerff\Ajax\Config::IBLOCK,
            'SECTION_NAME' => \Jerff\Ajax\Config::IBLOCK,
            'ELEMENT_NAME' => '',
        ),
        'en' => array(
            'NAME' => \Jerff\Ajax\Config::IBLOCK,
            'SECTION_NAME' => \Jerff\Ajax\Config::IBLOCK,
            'ELEMENT_NAME' => '',
        ),
    ),
);