<?php

namespace Jerff\Ajax;

class Config
{

    const MODULE_ID = 'jerff.ajax';
    const IBLOCK = 'jerff_ajax';
    const IBLOCK_FORM = 'jerff_ajax_form';
    const IBLOCK_AJAX = 'jerff_ajax';
    const FORM = 'jerff_ajax';

}